const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
const port = 4000;
const userRoute = require('./routes/user');
const productRoute = require('./routes/products');
const orderRoute = require('./routes/orders')

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/users', userRoute);
app.use('/products', productRoute);
app.use('/', orderRoute);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log(`Connected to database`))

app.listen(process.env.PORT || port, () => console.log(`API is now online on port ${process.env.PORT || port}`));