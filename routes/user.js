const express = require('express');
const router = express.Router();
const auth = require('../auth');
const userController = require('../controllers/user');

// User Registration
router.post('/register', (req, res) => {
    userController.userRegistration(req.body).then(result => res.send(result));
});

// User Authentication
router.post('/login', (req, res) => {
	userController.userLogin(req.body).then(result => res.send(result));
});

// Set User as Admin (Admin Only)
router.get("/:id", (req, res) => {
    userController.getSpecificUser(req.params.id).then(result => res.send(result));
})

router.patch("/:id/setAsAdmin", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    userController.setAsAdmin(req.params.id, {isAdmin: userData}).then(result => res.send(result));
})

// Make order
router.post('/checkout', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    userController.makeOrder(req.body, {isAdmin: userData}).then(result => res.send(result));
})

module.exports = router;