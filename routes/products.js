const express = require('express');
const router = express.Router();
const auth = require('../auth');
const productController = require('../controllers/product');

// Retrieve all active products
router.get('', (req, res) => {
    productController.activeProducts(req.body).then(result => res.send(result));
});

// Retrieve single product
router.get('/search', (req, res) => {
    productController.searchProduct(req.body).then(result => res.send(result));
})
router.get('/:id', (req, res) => {
    productController.searchProdId(req.params.id).then(result => res.send(result))
})

// Create new product (admin only)
router.post('', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    productController.addProduct(req.body, {isAdmin: userData}).then(result => res.send(result));
});

// Update Product Information (Admin Only)
router.patch("/:id", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    productController.editProduct(req.params.id, req.body, {isAdmin: userData}).then(result => res.send(result));
})

// Archive Product (Admin only)
router.patch("/:id/archive", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    productController.deactivateProduct(req.params.id, {isAdmin: userData}).then(result => res.send(result));
})

module.exports = router;