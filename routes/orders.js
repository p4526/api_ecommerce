const express = require('express');
const router = express.Router();
const auth = require('../auth');
const orderController = require('../controllers/order')

// Retrieve Auth User's Order (findById)
router.get('/user/:id/myOrders', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    orderController.showOrders(req.params.id, {isAdmin: userData}).then(result => res.send(result));
});

// retireve all order (Admin Only)
router.get('/user/orders', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    orderController.getAllOrders(req.body, {isAdmin: userData}).then(result => res.send(result));
});

module.exports = router;