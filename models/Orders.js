const mongoose = require('mongoose');

orderSchema = new mongoose.Schema({
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    products: [{
        productId: {
            type: String,
            required: true
        }
    }],
    totalAmount: {
        type: Number,
        required: true
    },
    userId: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Order', orderSchema)