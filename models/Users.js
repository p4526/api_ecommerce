const mongoose = require('mongoose');

userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, `email required`]
    },
    password: {
        type: String,
        required: [true, `password required`],
        select: false
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    firstName: {
        type: String,
        required: [true, `first name required`]
    },
    lastName: {
        type: String,
        required: [true, `last name required`]
    },
    mobileNo: {
        type: String,
        required: [true, `mobile number required`]
    },
    address: {
        type: String,
        required: [true, `address required`]
    },
    memberSince: {
        type: Date,
        default: new Date()
    },
    orders: [{
        productId: {
            type: String,
            required: true
        },
        status: {
            type: String,
            default: 'pending'
        }
    }]
})

module.exports = mongoose.model('User', userSchema)