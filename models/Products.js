const mongoose = require('mongoose');

productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, `product name required`]
    },
    description: {
        type: String,
        requried: [true, `product description required`]
    },
    price: {
        type: Number,
        required: [true, `product price required`]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Product', productSchema)