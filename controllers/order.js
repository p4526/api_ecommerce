const Users = require('../models/Users');
const Orders = require('../models/Orders');

// Retrieve Auth User's Order (findById)
module.exports.showOrders = (userId) => {
    return Users.findById(userId).then((result, err) => {
        if(err){
            return false
        } else {
            return result.orders
        }
    })
}

// retireve all order (Admin Only)
module.exports.getAllOrders = () => {
    return Orders.find().then((result, err) => {
        if(err){
            return false
        } else {
            return result
        }
    })
}