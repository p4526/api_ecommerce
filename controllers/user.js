const Users = require('../models/Users');
const Products = require('../models/Products');
const Orders = require('../models/Orders');
const auth = require('../auth');
const bcrypt = require('bcrypt');

// User Registration
module.exports.userRegistration = (reqBody) => {
    let newUser = new Users({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 12),
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        mobileNo: reqBody.mobileNo,
        address: reqBody.address
    });
    return Users.find({email: reqBody.email}).then(result => {
        if(result.length > 0){
            return false;
        } else {
            return newUser.save().then((user, err) => {
                (err) ? false : true;
            })
        };
    })
}

// User Authentication
module.exports.userLogin = (reqBody) => {
    return Users.find({ email: reqBody.email }).then(result => {
        if(result == null){
            return false
        } else {
            const isPasswordCorrect = bcrypt.compare(reqBody.password, result.password)

            if(isPasswordCorrect){
                return {accessToken: auth.createAccessToken(result)}
            } else {
                return false
            }
        }
    });
}

// Activity GET
module.exports.getSpecificUser = (userId) => {
    return Users.findById(userId).then((result, error) => {
        if(error){
            console.log(error)
            return false;
        } else {
            return result
        }
    })
}

// Set User as Admin (Admin Only)
module.exports.setAsAdmin = (userId) => {
    return Users.findById(userId).then((result, error) => {
        if(error){
            console.log(error)
            return false;
        } else {
            result.isAdmin = true;
            return result.save().then((updatedAdmin, err) => {
                if(err){
                    return false
                } else {
                    return updatedAdmin
                }
            })
        }
    })
}

// Add Order
module.exports.makeOrder = async (data) => {

	let isUserUpdated = await Users.findById(data.userId).then(user => {
        user.orders.push({productId: data.productId})
		return user.save().then((user, err) => {
			if(err){
                return false
            } else {
                return true
            }
		})
	})

    let productOrdered = () => {
        return Products.findById(data.productId).then((result, err) => {
            if(err){
                return false
            } else {
                return result
            }
        })
    }

    let newOrder = await new Orders({
        userId: data.userId,
        products: [{
            productId: data.productId
        }],
        totalAmount: productOrdered.price
    })

    return newOrder.save()
}