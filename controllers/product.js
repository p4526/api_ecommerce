const Products = require('../models/Products');

// Retrieve all active products
module.exports.activeProducts = (reqBody) => {
    return Products.find({
        isActive: true
    }).then((result, err) => {
        if(err){
            return false
        } else {
            return result
        }
    })
}

// Retrieve single product
module.exports.searchProduct = (reqBody) => {
    return Products.findOne({
        name: reqBody.name
    }).then((result, err) => {
        if(err){
            return false
        } else {
            return result
        }
    })
}
// Retrieve single product via ID
module.exports.searchProdId = (prodId) => {
    return Products.findById(prodId).then((res, err) => {
        if(err){
            return false            
        } else {
            return res
        }
    })
}

// Create new product (admin only)
module.exports.addProduct = (reqBody) => {
    let newProduct = new Products({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });
    return Products.find({name: reqBody.name}).then(result => {
        if(result.length > 0){
            return false;
        } else {
            return newProduct.save().then((product, err) => {
                if(err){
                    return false
                } else {
                    return product
                }
            })
        };
    })
}

// Update Product Information (Admin Only)
module.exports.editProduct = (productId, reqBody) => {
    return Products.findById(productId).then((result, error) => {
        if(error){
            return false;
        } else {

            result.name = reqBody.name;
            result.description = reqBody.description;
            result.price = reqBody.price;

            return result.save().then((updatedInfo, err) => {
                if(err){
                    return false
                } else {
                    return updatedInfo
                }
            })
        }
    })
}

// Archive Product (Admin only)
module.exports.deactivateProduct = (productId) => {
    return Products.findById(productId).select(Products.name=0, Products.description=0).then((result, error) => {
        if(error){
            return false;
        } else {
            result.isActive = false;
            return result.save().then((updatedProduct, err) => {
                if(err){
                    return false
                } else {
                    return updatedProduct
                }
            })
        }
    })
}